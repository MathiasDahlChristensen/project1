*Problem 2 - The key management problem*

Sets
k set of keys /k1*k30/
n set of nodes /n1*n8/
;

*Make alias of n for later use (to compare which keys two different nodes uses)
alias(n,s);


scalar
q Restriction on minimum number of keys to share to construct direct connection /3/
T max number of times key k can be used /2/
m space key k occupies (could be a parameter if the keys took diff. space /186/
p space limit for node n (same as for scalar m) /1000/
;

*We need to keep track of whether node n contains key k, whether node n and s are
*directly connected and if node n and s share key k:
Binary variable
y(n,k) 1 if node n has key k 0 otherwise
x(n,s,k) 1 if node n and s share key k 0 otherwise
delta(n,s) 1 if node n and s are directly connected
;


*Need variable to keep track of objective function value
Variable
xi objective function value
;


Equations
Total Objective function
Node_capacity(n) Constraint specifying the amount of keys a node can hold
Max_keys(k) Number of times a key can be used
Count_key_match(n,s,k) constraint on x
Delta_count(n,s) constraint counting deltas
;

*Gives the total number of direct connections (counts whenever node n and s shares 3
*or more keys, captured in the variable delta). Note that we only count one time
*if node 1 and node 2 has a direct connection (delta(1,2) and delta(2,1) is only
*counted one time)
Total .. xi =e= sum(n, sum(s$(ord(s)>ord(n)),delta(n,s)));

*Makes sure each node takes a max number of keys in line with its capacity.
Node_capacity(n) .. sum(k, y(n,k))*m =l= p;

*Makes sure that each key, summed over all the nodes, can be used at max T times.
Max_keys(k) .. sum(n, y(n,k)) =l= T;

*As we are trying to maximize the amount of keys node n and s shares (at least up
*up to q times), this constraint makes sure that x only can be 1 when node n and s
*share key k. If only one of the two is 1, x is forced to be zero, and the
*restriction will still be satisfied (the same if both nodes do not contain key k).
*As we are checking this for all nodes and all keys we have to control these sets.
*We take order s greater than n to make sure we don't count twice if node n and s
*shares key k.
Count_key_match(n,s,k)$(ord(s)>ord(n)) .. 2*x(n,s,k)-y(n,k) =l= y(s,k);

*As we are trying to maximize the number of times delta is 1, the RHS will take
*value q (meaning delta(n,s)=1) whenever to nodes shares a minimum of q keys. Else
*it is forced to zero. We take order s greater than n to make sure we don't
*count any direct connection twice.
Delta_count(n,s)$(ord(s)>ord(n)) .. sum(k, x(n,s,k)) =g= q*delta(n,s);


Model keyproblem /all/;
*Solving the problem as a Mixed Integer Problem, where we are maximizing the
*number of direct connections, e.g. number of nodes sharing 3 or more keys.
Solve keyproblem using MIP maximizing xi;
Display xi.l, y.l, x.l, delta.l;


