*Problem 1 - Optimal Formation*

*First we need to distinguish between players (and a subset of these),
*the positions and formations
Sets
    s set of players on the team / S1*S25 /
    p positions the players can take / GK, CDF, LB, RB, CMF, LW, RW, OMF, CFW, SFW /
    f 'formations' / 442, 352, 4312, 433, 343, 4321 /
    subqp(s) 'subset of quality players' /S13, S20, S21, S22 /
    subsp(s) 'subset of strength players' /S10, S12, S23 /;

*Now we attain values according to the players in different positions (from 0-10)
Table Q(s,p) quality of player s in position p
            GK          CDF         LB          RB          CMF         LW          RW          OMF         CFW         SFW
S1          10          0           0           0           0           0           0           0           0           0    
S2          9           0           0           0           0           0           0           0           0           0
S3          8.5         0           0           0           0           0           0           0           0           0
S4          0           8           6           5           4           2           2           0           0           0         
S5          0           9           7           3           2           0           2           0           0           0
S6          0           8           7           7           3           2           2           0           0           0
S7          0           6           8           8           0           6           6           0           0           0          
S8          0           4           5           9           0           6           6           0           0           0
S9          0           5           9           4           0           7           2           0           0           0
S10         0           4           2           2           9           2           2           0           0           0           
S11         0           3           1           1           8           1           1           4           0           0
S12         0           3           0           2           10          1           1           0           0           0
S13         0           0           0           0           7           0           0           10          6           0
S14         0           0           0           0           4           8           6           5           0           0           
S15         0           0           0           0           4           6           9           6           0           0
S16         0           0           0           0           0           7           3           0           0           0
S17         0           0           0           0           3           0           9           0           0           0
S18         0           0           0           0           0           0           0           6           9           6
S19         0           0           0           0           0           0           0           5           8           7
S20         0           0           0           0           0           0           0           4           4           10
S21         0           0           0           0           0           0           0           3           9           9
S22         0           0           0           0           0           0           0           0           8           8
S23         0           3           1           1           8           4           3           5           0           0
S24         0           3           2           4           7           6           5           6           4           0
S25         0           4           2           2           6           7           5           2           2           0;


*We also need to make restrictions to how many players needed in the
*different positions in the different formations.
Table b(f,p) players needed for position p in formation f
            GK          CDF         LB          RB          CMF         LW          RW          OMF         CFW         SFW
442         1           2           1           1           2           1           1           0           2           0
352         1           3           0           0           3           1           1           0           1           1           
4312        1           2           1           1           3           0           0           1           2           0        
433         1           2           1           1           3           0           0           0           1           2
343         1           3           0           0           2           1           1           0           1           2
4321        1           2           1           1           3           0           0           2           1           0;


*We need binary variables to keep track of whether player s is chosen or not
*and to keep track of if formation f is chosen
Binary Variables
x(s,p,f) counts 1 if player s is chosen for position p in formation f and 0 otherwise
y(f) indicates if formation f is chosen;

*Need a variable that shows us the objective value function
Variables
xi objective function value

Equations
total objective function
One_Position(s,f) one player can at max take one position
One_Formation exactly one formation is chosen
Fill_Positions(f,p) every position in every formation is filled
Quality_Player(f) minimal one quality player
Strength_Player(f) minimal one strength player if four quality players;

*Has the purpose of keeping track of the objective function value, namely
*the quality of the total team in the chosen formation:
total .. xi =e= sum(f,sum(p,sum(s, Q(s,p)*x(s,p,f))));

*Summing over the positions, this constraint keeps track of one player not
*being assigned to more than one position. If the player s is not assigned to
*a position in formation f, the LHS is 0 (which still satisfies the constraint).
*Note that the constraint need to be satisfied for all players and all formations,
*which is fine as we can only play 1 formation.
One_Position(s,f) .. sum(p, x(s,p,f)) =l= 1;

*Makes sure we only choose exactly one formation.
One_formation .. sum(f, y(f)) =e= 1;

*The amount of players needed for a position in a formation needs to be filled in.
*Notice that the RHS is zero for every formation that is not chosen.
*As the problem is a maximization problem the constraint could be "less-than or
*equal to", and we would still get the same result.
Fill_Positions(f,p) .. sum(s, x(s,p,f)) =e= b(f,p)*y(f);

*Constraint making sure we get at least one quality player in the formation
Quality_Player(f) .. sum(p,sum(subqp(s), x(s,p,f))) =g= y(f);

*Constraint making sure that at least one strength player is chosen (sum on LHS is
*at least one) if all quality players are chosen.
Strength_Player(f) .. sum(p,sum(subqp(s), x(s,p,f))) =l= (sum(p,sum(subsp(s),x(s,p,f)))+card(subqp)-1);


Model football /all/;
*Solving the problem as a Mixed Integer Problem, where we are maximizing the
*quality of the team.
Solve football using MIP maximizing xi;
Display xi.l, x.l, y.l;



