*Problem 3 - Fleet planning*

Sets
v set defining the ships /v1*v5/
p set defining the ports /Singapore, Incheon, Shanghai, Sydney, Gladstone, Dalian, Osaka, Victoria/
r set defining the routes /Asia, ChinaPacific/
subSO(p) Subset of ports that can't be used at the same time /Singapore, Osaka/
subIV(p) Subset of ports that can't be used at the same time /Incheon, Victoria/
;

Scalar
k number of ports needed to be serviced /5/
;


Parameters
F(v) Initial costs of using ship v to service /v1 65, v2 60, v3 92, v4 100, v5 110/
G(v) Days ship v can service in a year /v1 300, v2 250, v3 350, v4 330, v5 300/
D(p) Number of times port p needs to be visited /Singapore 15, Incheon 18, Shanghai 32, Sydney 32, Gladstone 45, Dalian 32, Osaka  15, Victoria 18/
;


Table
C(v,r) Costs for ship v of taking route r
    Asia    ChinaPacific
v1  1.41    1.9
v2  3.0     1.5
v3  0.4     0.8
v4  0.5     0.7
v5  0.7     0.8
;


Table
T(v,r) Days it takes ship v to complete route r
    Asia    ChinaPacific
v1  14.4    21.2
v2  13.0    20.7
v3  14.4    20.6
v4  13.0    19.2
v5  12.0    20.1
;


Table
A(r,p) Indicates whether route r visits port p (1 if visited 0 otherwise)
                Singapore   Incheon Shanghai    Sydney  Gladstone   Dalian  Osaka   Victoria
Asia            1           1       1           0       0           1       1       0
ChinaPacific    0           0       1           1       1           1       0       1   
;

*We need to keep track of whether ship v is used or not (same for port p)
Binary Variables
x(v) Indicates whether ship v is used or not
y(p) Indicates whether port p is used or not
;


Integer variables
z(v,r) counts the number of times ship z completes route r
;


Free variable
xi objective function value
;


Equations
Total Objective function
Min_Ports Minimum number of ports needed to be used
Max_days(v) Maximum number of days ship v can be used
Min_delivery(p) Minimum amount of deliveries port p needs
Destination_constraint1 If Singapore is used the Osaka can't be used and vice versa
Destination_constraint2 If Incheon is used the Victoria can't be used and vice versa
Initialcost1(v) Make x(v) zero if the ship is not used
Initialcost2(v) Make x(v) one if the ship is used
;

*Objective function takes the sum of initial value of ship v (if it is used) and
*adds the number of times ship v takes route r times the cost for ship v taking
*route r. Note that if ships v is not used both x og z are zero and therefore has
*no contribution to the objective function value.
Total .. xi =e= sum(v, F(v)*x(v))+sum((v,r), C(v,r)*z(v,r));

*Makes sure we service at least K ports.
Min_Ports .. sum(p, y(p)) =g= K;

*Makes sure each ship are in service at max the number of days it was initally
*assigned (the variable G(v)).
Max_days(v) .. sum(r, T(v,r)*Z(v,r)) =l= G(v);

*Makes sure that the ports chosen gain the amount of services they are demanding.
*Note that the RHS is zero if port p is not chosen and as we want to minimize z,
*the sum on the LHS will be zero. Also note that we distinguish between what route
*ship v has to use by multiplying the LHS with A (which is an indicator of whether
*port p is in route r or not). This means that if port p is not in route r, then
*the sums over this specific route has to be zero, and any contributions has to
*come from the other route.
Min_delivery(p) .. sum((r,v), Z(v,r)*A(r,p)) =g= D(p)*y(p);

*Makes sure that we dont use both Singapore and Osaka as ports to service
Destination_constraint1 .. sum(subSO(p), y(p)) =l= 1;

*Makes sure that we dont use both Incheon and Victoria as ports to service
Destination_constraint2 .. sum(subIV(p), y(p)) =l= 1;

*Makes sure that the initial cost is not paid if we don't use ship v in any routes
*(note that if z is zero then x has to be zero too).
Initialcost1(v) .. x(v) =l= sum(r, z(v,r));

*Makes sure that the initial cost of using ship v is paid whenever we use ship v in
*at least one of the routes. The 100.000 is an arbitrary upper bound, and could have
*been any other (sufficient) high number. If z is one or greater, then x is forced
*to be one.
Initialcost2(v) .. x(v)*100000 =g= sum(r, z(v,r));


Model ships /all/;
*Solving the problem as a Mixed Integer Problem, where we are minimizing the
*total costs.
Solve ships using MIP minimizing xi;
Display xi.l, x.l, y.l, z.l


